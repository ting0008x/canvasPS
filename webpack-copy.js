/**
 * 构建脚本
 */
const path = require('path');
const { exec } = require('child_process');
const out_dir = './dist'
const out_file = 'CanvasPs.js'
const asset_dir = './asset'
const ConfigData = {
    canvasps_file: out_file,
    canvasui_file: 'ui.js'
}

const fs = require('fs')
/**
 * 资源拷贝
 */
module.exports = class CopyPlugin {
    constructor(assets = 'assets', out = 'assets') {
        this.basePath = ''
        this.assets = assets
        this.out = out
    }
    apply(compiler) {
        this.basePath = compiler.options.output.path
        const src_path = path.resolve(__dirname, this.assets)
        const taget_path = path.resolve(this.basePath, this.out)
        //目录
        if (!fs.existsSync(taget_path)) {
            fs.mkdirSync(taget_path)
        }
        compiler.hooks.done.tap('CopyPlugin', () => {

            this.copy_dir(src_path, taget_path)
        })

    }
    exec_cmd(cmd) {
        return new Promise((resolve, reject) => {
            exec(cmd, { windowsHide: true }, (err, std) => {
                if (err) {
                    reject(cmd)
                    return
                }
                resolve(std)
            })
        })
    }
    //压缩文档
    compress(txt) {
        const reg = /(\n|\r|\t|\s{1,})/g
        return txt.replace(reg, ' ')
    }
    //复制目录
    copy_dir(src, target) {

        if (!fs.existsSync(src)) {
            console.error(src + '目录不存在')
            return
        }
        if (!fs.existsSync(target)) {
            fs.mkdirSync(target)
        }
        fs.readdir(src, (err, files) => {
            if (err) {
                console.error(src + '目录读取失败')
                console.error(err)
                return
            }
            for (let i = 0; i < files.length; i++) {
                fs.stat(src + '/' + files[i], (err, sta) => {
                    if (err) {
                        console.log(err)
                        return
                    }
                    if (sta.isDirectory()) {
                        //目录
                        this.copy_dir(src + '/' + files[i], target + '/' + files[i])
                    } else {
                        //文件
                        fs.copyFileSync(src + '/' + files[i], target + '/' + files[i])
                    }
                })

            }
        })
    }
    //清空目录
    del_dir(dir_path) {
        if (!fs.existsSync(dir_path)) {
            return
        }
        const sta = fs.statSync(dir_path)
        if (sta.isDirectory()) {
            const list = fs.readdirSync(dir_path)
            if (list.length == 0) {
                fs.rmdirSync(dir_path)
            } else {
                for (let i = 0; i < list.length; i++) {
                    del_dir(dir_path + '/' + list[i])
                }
            }
        } else {
            fs.unlinkSync(dir_path)
        }
    }
    //模板处理
    parse_template() {
        console.log('模板处理')
        fs.readFile(template, 'utf8', (err, data) => {
            if (err) {
                console.error('html目标读取失败')
                console.error(err)
                return
            }
            const reg = /\[\[=[\w\d]+\]\]/g
            const list = data.match(reg)
            if (list) {
                for (let i = 0; i < list.length; i++) {
                    const temp = list[i].replace(/[\[\]=]/g, '')
                    if (ConfigData[temp] === undefined) {
                        console.log(temp + '模板变量不存在')
                        continue
                    }
                    data = data.replace(list[i], ConfigData[temp])
                }
            }
            fs.writeFileSync(out_dir + '/' + template, data)
        })
    }
}
